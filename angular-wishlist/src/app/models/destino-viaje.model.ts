import {v4 as uuid} from 'uuid';

export class DestinoViaje {
    [x: string]: any;
  public servicios: string[];
  private seleted: boolean;
  public id = uuid();

  constructor(public nombre:string, public u:string, public votes: number = 0) {
      this.servicios = ['piscina', 'desayuno'];
    }
    isSelected() {
        return this.seleted;
    }

    setSelected(s: boolean) {
        this.seleted = s;
    }
    voteDown() {
      this.votes--;
    }
    voteUp() {
      this.votes++;
    }
   
}